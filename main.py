import sys
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import os
import re
import pyperclip
from PIL import Image, ImageTk

###############################################################
#
#              1.欢迎PR，一起继续美化GUI
#              2.后续exe打包流程会放在Gitee 或者GitHub
#              3.代码开源，但是不要用于商业化嗷
#              4.为了秧秧，作者正在持续发电中
#
###############################################################


# 文件路径用于保存游戏安装路径
config_file = "game_path.txt"

# 初始化游戏安装路径
game_path = ""
path_label = None  # 定义 path_label 为全局变量
link_label = None  # 定义 link_label 为全局变量
source_link = "https://gitee.com/limaoyi/wave_gacha_query_gui"  # 添加的链接


# --------------------- 获取 URL -------------------------

def get_url(game_path):
    log_file = os.path.join(game_path, "Client", "Saved", "Logs", "Client.log")
    if not os.path.exists(log_file):
        return "文件 '{}' 不存在。请确保游戏安装路径设置正确。".format(log_file)

    # 读取日志文件内容
    with open(log_file, 'r', encoding='utf-8') as file:
        log_content = file.read()

    # 搜索指定的 URL
    url_pattern = r'url":"(https://aki-gm-resources(?:-oversea)?\.aki-game\.(?:net|com).*?)"'
    matches = re.findall(url_pattern, log_content)

    # 如果找到 URL
    if matches:
        url = matches[-1]  # 取最后一个匹配项
        pyperclip.copy(url)  # 将 URL 复制到剪贴板
        return "抽卡记录 URL:\n {}\n已自动复制到剪贴板。请将其粘贴到秧秧小助手抽卡分析输入框，并点击开始分析！".format(url)
    else:
        return "在日志文件中未找到匹配项。请先打开抽卡历史！"


# -------------------------------------------------------------

def run_script():
    global game_path
    if not game_path:
        messagebox.showerror("错误", "请先选择游戏安装路径。")
        return

    try:
        # 调用获取 URL 函数并获取结果
        result = get_url(game_path)

        # 清空文本框内容并更新结果
        output_textbox.delete(1.0, tk.END)
        output_textbox.insert(tk.END, result)
    except Exception as e:
        messagebox.showerror("错误", f"执行脚本时发生错误: {e}")


def choose_game_path():
    global game_path
    game_path = filedialog.askdirectory(title="选择游戏安装路径")
    if game_path:
        path_label.config(text=f"当前路径: {game_path}")
        # 保存游戏路径到文件
        save_game_path(game_path)


def save_game_path(path):
    with open(config_file, "w") as file:
        file.write(path)


def load_game_path():
    global game_path
    global path_label  # 声明 path_label 为全局变量
    if os.path.exists(config_file):
        with open(config_file, "r") as file:
            game_path = file.read().strip()
            if game_path and path_label:  # 检查 path_label 是否存在
                path_label.config(text=f"当前路径: {game_path}")


# 创建主窗口
root = tk.Tk()
root.title("秧秧小助手获取抽卡链接 URL")
root.geometry("400x600")

# 创建按钮，点击后选择游戏路径
path_button = tk.Button(root, text="第一次使用： 选择游戏安装路径", command=choose_game_path)
path_button.pack(pady=10)

# 显示当前选择的路径
path_label2 = tk.Label(root, text="提示：请选择 Wuthering Waves Game 这个文件夹！")
path_label2.pack(pady=5)

# 显示当前选择的路径
path_label = tk.Label(root, text="例如 E:/Wuthering Waves/Wuthering Waves Game")
path_label.pack(pady=5)

# 加载上次保存的游戏路径
load_game_path()

# 创建按钮，点击后执行脚本
button = tk.Button(root, text="开始获取抽卡链接", command=run_script)
button.pack(pady=20)

# 创建文本框，用于显示结果
output_textbox = tk.Text(root, wrap=tk.WORD, height=10, width=50)
output_textbox.pack(pady=20)

# 添加使用方法文案
instructions = tk.Label(
    root,
    text=("使用方法：\n1. 打开游戏客户端。\n"
          "2. 打开抽卡记录，随机查询一到两个卡池历史记录。\n"
          "3. 点击 '开始获取' 按钮来获取抽卡链接。"),
    justify=tk.LEFT,
    wraplength=380
)
instructions.pack(pady=10)

# 添加跳转链接
link_url = "https://gitee.com/limaoyi/wave_gacha_query_gui"  # 请替换为你的链接
link_label = tk.Label(root, text="点击这里查看项目开源地址", fg="blue", cursor="hand2")
link_label.pack(pady=10)
link_label.bind("<Button-1>", lambda e: open_link(link_url))


# 获取当前目录或打包后资源的临时路径
def get_resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


# 设置图片路径
image_path = get_resource_path("wave.png")  # 使用相对路径，打包时包含文件

try:
    # 检查图片文件是否存在
    if not os.path.exists(image_path):
        messagebox.showerror("错误", f"图片文件 '{image_path}' 不存在，请检查文件路径。")
    else:
        try:
            im = Image.open(image_path)

            # 设置缩放的宽度和高度
            scale_width = 352  # 目标宽度
            scale_height = 130  # 目标高度

            # 缩放图片
            im_resized = im.resize((scale_width, scale_height), Image.ANTIALIAS)
            photo = ImageTk.PhotoImage(im_resized)

            img_label = tk.Label(root, image=photo)
            img_label.image = photo  # 保持对图片对象的引用，以防被垃圾回收
            img_label.pack(side=tk.BOTTOM, pady=10)  # 放置图片在窗口底部

        except Exception as e:
            messagebox.showerror("错误", f"加载图片时发生错误: {e}")
except Exception as e:
    messagebox.showerror("错误", f"图片加载时发生错误: {e}")


# 打开链接函数
def open_link(url):
    import webbrowser
    webbrowser.open_new(url)


# 运行主循环
root.mainloop()
